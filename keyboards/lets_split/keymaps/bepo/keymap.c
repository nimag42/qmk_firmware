#include "lets_split.h"
#include "action_layer.h"
#include "eeconfig.h"
#include "keymap_french.h"

extern keymap_config_t keymap_config;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
#define _BEPO 0
#define _BEPO_A 1 // Bépo with ALT_GR
#define _BEPO_S 2 // Bépo with shift
#define _AZERTY 3
#define _LOWER 4
#define _RAISE 5
#define _ADJUST 16

enum custom_keycodes {
  ALTGR,
  BEPO = SAFE_RANGE,
  BEPO_A,
  BEPO_S,
  AZERTY,
  LOWER,
  RAISE,
  ADJUST,
};

// Fillers to make layering more clear
#define _______ KC_TRNS
#define XXXXXXX KC_NO

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  /* Bépo
 * ,------------------------------------------ ------------------------------------------.
 * | Tab  |   B  |   É  |   P  |   O  |   W  | |   ^  |   V  |   D  |   L  |   J  | Bksp | Z W
 * |------+------+------+------+------+------- +------+------+------+------+------+------|
 * | AltGr|   A  |   U  |   I  |   E  |   ,  | |   C  |   T  |   S  |   R  |   N  |  M   | Ç
 * |------+------+------+------+------+------| +------+------+------+------+------+------|
 * | Shift|   Z  |   Y  |   X  |   .  |   K  | |   '  |   Q  |   G  |   H  |   F  |Enter |
 * |------+------+------+------+------+------+ +------+------+------+------+------+------|
 * | Esc  |Raise |Lower |  GUI | Ctrl |Space | |Space | Alt  | Left | Down |  Up  |Right |
 * `------------------------------------------ ------------------------------------------'
 */
[_BEPO] = KEYMAP( \
  KC_TAB,  KC_B,    FR_EACU, KC_P,    KC_O,    FR_W,    KC_LBRC, KC_V,    KC_D,    KC_L,    KC_J,    KC_BSPC, \
  ALTGR,   FR_A,    KC_U,    KC_I,    KC_E,    FR_COMM, KC_C,    KC_T,    KC_S,    KC_R,    KC_N,    FR_M, \
  KC_LSFT, FR_Z,    KC_Y,    KC_X,    FR_DOT,  KC_K,    FR_APOS, FR_Q,    KC_G,    KC_H,    KC_F,    KC_ENT , \
  KC_ESC,  RAISE,   LOWER,   KC_LALT, KC_LCTRL,KC_SPC,  KC_SPC,  KC_LGUI, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT \
),

/* Bépo ALT GR
 * ,------------------------------------------ ------------------------------------------.
 * | Esc  |   à  |   ç  |   &  |  |   |   È  | |  -   |  {   |  }   |  +   |  @   | Del  |
 * |------+------+------+------+------+------- +------+------+------+------+------+------|
 * | Tab  |   ù  |   <  |   >  |   /  |   €  | |  _   |  (   |  )   |  =   |  $   |  %   |
 * |------+------+------+------+------+------| +------+------+------+------+------+------|
 * | Shift|   Œ  |   ~  |   `  |   \  |  ¨   | |  "   |  [   |   ]  |  *   |  #   |Enter |
 * |------+------+------+------+------+------+ +------+------+------+------+------+------|
 * | AltGr|Raise |Lower |  GUI | Ctrl |Space | |Space | Alt  | Left | Down |  Up  |Right |
 * `------------------------------------------ ------------------------------------------'
 */
[_BEPO_A] = KEYMAP( \
  KC_TAB,  FR_AGRV,   FR_CCED,  FR_AMP,  FR_PIPE, FR_EGRV,  FR_MINS, FR_LCBR, FR_RCBR, FR_PLUS, FR_AT,  KC_DEL, \
  ALTGR,   FR_UGRV,   FR_LESS,  FR_GRTR, FR_SLSH, FR_EURO,  FR_UNDS, FR_LPRN, FR_RPRN, FR_EQL,  FR_DLR, FR_PERC, \
  KC_LSFT,S(FR_UGRV),FR_TILD,  FR_GRV,  FR_BSLS,S(KC_LBRC),FR_QUOT, FR_LBRC, FR_RBRC, FR_ASTR, FR_HASH,KC_ENT , \
  KC_ESC,  RAISE,     LOWER,    KC_LALT, KC_LCTRL, KC_SPC,  KC_SPC,  KC_LGUI, KC_LEFT, KC_DOWN, KC_UP,  KC_RGHT \
),

/* Bépo SHIFT
 * ,------------------------------------------ ------------------------------------------.
 * | Tab  |      |      |      |      |      | |  !   |      |      |      |      | Bksp | Z W
 * |------+------+------+------+------+------- +------+------+------+------+------+------|
 * | Altgr|      |      |      |      |  ;   | |      |      |      |      |      |      | Ç
 * |------+------+------+------+------+------| +------+------+------+------+------+------|
 * | Shift|      |      |      |   :  |      | |  ?   |      |      |      |      |Enter |
 * |------+------+------+------+------+------+ +------+------+------+------+------+------|
 * | Esc  |Raise |Lower |  GUI | Ctrl |Space | |Space | Alt  | Left | Down |  Up  |Right |
 * `------------------------------------------ ------------------------------------------'
 */
[_BEPO_S] = KEYMAP( \
S(KC_TAB), S(KC_B), S(FR_EACU), S(KC_P), S(KC_O), S(KC_W), FR_EXLM, S(KC_V), S(KC_D), S(KC_L), S(KC_J), KC_BSPC, \
  ALTGR,   S(FR_A), S(KC_U),    S(KC_I), S(KC_E), FR_SCLN, S(KC_C), S(KC_T), S(KC_S), S(KC_R), S(KC_N), S(FR_M), \
  KC_LSFT, S(FR_Z), S(KC_Y),    S(KC_X), FR_COLN, S(KC_K), FR_QUES, S(FR_Q), S(KC_G), S(KC_H), S(KC_F), KC_ENT , \
  KC_TAB,  RAISE,   LOWER,   KC_LALT,    KC_LCTRL,KC_SPC,  KC_SPC,  KC_LGUI, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT \
),

/* AZERTY
* ,------------------------------------------ ------------------------------------------.
 * | Tab  |   A  |  Z   |      |      |      | |  !   |      |      |      |      | Bksp |
 * |------+------+------+------+------+------- +------+------+------+------+------+------|
 * | Altgr|      |      |      |   ;  |      | |      |      |      |      |      |      |
 * |------+------+------+------+------+------| +------+------+------+------+------+------|
 * | Shift|      |      |      |   :  |      | |  ?   |      |      |      |      |Enter |
 * |------+------+------+------+------+------+ +------+------+------+------+------+------|
 * | Esc  |Raise |Lower |  GUI | Ctrl |Space | |Space | Alt  | Left | Down |  Up  |Right |
 * `------------------------------------------ ------------------------------------------'
 */
[_AZERTY] = KEYMAP( \
  KC_TAB,  FR_A,    FR_Z,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSPC, \
  ALTGR,   FR_Q,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    FR_M,    KC_QUOT, \
  KC_LSFT, FR_W,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    FR_COMM, FR_SCLN, FR_COLN, FR_EXLM, KC_ENT , \
  KC_TAB,  RAISE,   LOWER,   KC_LALT, KC_LCTRL,KC_SPC,  KC_SPC,  KC_LGUI, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT \
),

/* Lower
 * ,-----------------------------------------------------------------------------------.
 * |   ~  |   &  |   é  |   "  |   '  |   (  |   -  |   è  |   _  |   ç  |   à  | Del  |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * | Del  |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |   _  |   +  |      |   \  |  |   |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |  F7  |  F8  |  F9  |  F10 |  F11 |  F12 |ISO ~ |ISO | |      |      |Enter |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |             |      | Next | Vol- | Vol+ | Play |
 * `-----------------------------------------------------------------------------------'
 */
[_LOWER] = KEYMAP( \
  KC_TILD, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0, KC_DEL, \
  KC_DEL,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_PIPE, \
  _______, KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,S(KC_NUHS),S(KC_NUBS),_______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, KC_MNXT, KC_VOLD, KC_VOLU, KC_MPLY \
),

/* Raise
 * ,-----------------------------------------------------------------------------------.
 * |   `  |   1  |   2  |   3  |   4  |   5  |   6  |   7  |   8  |   9  |   0  | Del  |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * | Del  |   4  |   5  |   6  |      |      |      |      |      |   ^  |      |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |   7  |   8  |   9  |      |      |      |      |   <- |   d  |  ->  |Enter |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |             |      |      |      |      |      |
 * `-----------------------------------------------------------------------------------'
 */
[_RAISE] = KEYMAP( \
  KC_GRV,  FR_1,   FR_2,   FR_3,   FR_4,    FR_5,    FR_6,    FR_7,    FR_8,    FR_9,    FR_0,    KC_DEL, \
  KC_DEL,  FR_4,   FR_5,   FR_6,   _______,   _______,   _______,   _______, _______,  KC_UP, _______, _______, \
  _______, FR_7,   FR_8,   FR_9,   _______,  _______,  _______,  _______, KC_LEFT, KC_DOWN, KC_RGHT, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______ \
),

/* Adjust (Lower + Raise)
 * ,-----------------------------------------------------------------------------------.
 * |      | Reset|      |      |      |      |      |      |      |      |      |      |
 * |------+------+------+------+------+-------------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      | Bépo |      |
 * |------+------+------+------+------+------|------+------+------+------+------+------|
 * |      |      |      |      |      |      |      |      |      |      |Azerty|      |
 * |------+------+------+------+------+------+------+------+------+------+------+------|
 * |      |      |      |      |      |             |      |      |      |      |      |
 * `-----------------------------------------------------------------------------------'
 */
[_ADJUST] =  KEYMAP( \
  _______, RESET,   _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, BEPO,    _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, AZERTY,  _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______ \
)


};

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case KC_LSFT:
      if (record->event.pressed) {
        layer_on(_BEPO_S);
      } else {
        layer_off(_BEPO_S);
      }
      return false;
      break;      
    case ALTGR:
      if (record->event.pressed) {
        layer_on(_BEPO_A);
      } else {
        layer_off(_BEPO_A);
      }
      return false;
      break;      
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
	update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
	update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
    case AZERTY:
      if (record->event.pressed) {
        persistent_default_layer_set(1UL<<_AZERTY);
      }
      return false;
      break;
    case BEPO:
      if (record->event.pressed) {
        persistent_default_layer_set(1UL<<_BEPO);
      }
      return false;
      break;
  }
  return true;
}

