#include "iris.h"
#include "action_layer.h"
#include "eeconfig.h"
#include "keymap_french.h"

extern keymap_config_t keymap_config;

#define _QWERTY 0
#define _BEPOW 1
#define _BEPOW_S 2
#define _LOWER 10
#define _RAISE 11
#define _ADJUST 16

enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  LOWER,
  RAISE,
  ADJUST,
  BEPO,
  BEPOS,
};

#define KC_ KC_TRNS
#define _______ KC_TRNS
#define ______ KC_TRNS

#define KC_LOWR LOWER
#define KC_RASE RAISE
#define KC_RST RESET
#define KC_BL_S BL_STEP

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [_BEPOW] = KEYMAP(
//,------+------+------+------+------+------.                ,------+------+------+------+------+------.
   KC_ESC, FR_1 , FR_2 , FR_3 , FR_4 , FR_5 ,                  FR_6 , FR_7 , FR_8 , FR_9 , FR_0 , RAISE,
//|------+------+------+------+------+------|                |------+------+------+------+------+------|
   KC_UP, KC_B,FR_EACU, KC_P , KC_O , FR_W ,                 KC_LBRC, KC_V , KC_D , KC_L , KC_J ,KC_LEFT,
//|------+------+------+------+------+------|                |------+------+------+------+------+------|
   BEPOS , FR_A , KC_U , KC_I , KC_E ,FR_COMM,                 KC_C , KC_T , KC_S , KC_R , KC_N , FR_M ,
//|------+------+------+------+------+------+------.  ,------+------+------+------+------+------+------|
  KC_DOWN, FR_Z , KC_Y , KC_X ,FR_DOT, KC_K , KC_TAB, KC_LGUI,FR_APOS,FR_Q , KC_G , KC_H , KC_F ,KC_RGHT,
//|------+------+------+------+------+------+------/  \------+------+------+------+------+------+------|
                              LOWER,KC_LCTL,KC_SPC,    KC_ENT,KC_LALT,KC_BSPC
//                                `----+----+----'      `----+----+----'
  ),

  [_BEPOW_S] = KEYMAP(
//,------+------+------+------+------+------.                ,------+------+------+------+------+------.
   ______, KC_1 , KC_2 , KC_3 , KC_4 , KC_5 ,                  KC_6 , KC_7 , KC_8 , KC_9 , KC_0 ,______,
//|------+------+------+------+------+------|                |------+------+------+------+------+------|
   ______,S(KC_B),RGUI(RALT(S(FR_EACU))),S(KC_P),S(KC_O),S(FR_W),        FR_EXLM,S(KC_V),S(KC_D),S(KC_L),S(KC_J),______,
//|------+------+------+------+------+------|                |------+------+------+------+------+------|
   ______,S(FR_A),S(KC_U),S(KC_I),S(KC_E),FR_SCLN,           S(KC_C),S(KC_T),S(KC_S),S(KC_R),S(KC_N),S(FR_M),
//|------+------+------+------+------+------+------.  ,------+------+------+------+------+------+------|
   ______,S(FR_Z),S(KC_Y),S(KC_X),FR_COLN,S(KC_K),______,______,FR_QUES,S(FR_Q),S(KC_G),S(KC_H),S(KC_F),______,
//|------+------+------+------+------+------+------/  \------+------+------+------+------+------+------|
                              ______,______,______,    ______,______,______
//                                `----+----+----'      `----+----+----'
  ),

  [_QWERTY] = KC_KEYMAP(
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
     ESC , 1  , 2  , 3  , 4  , 5  ,                6  , 7  , 8  , 9  , 0  ,RASE,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     UP , Q  , W  , E  , R  , T  ,                Y  , U  , I  , O  , P  ,LEFT,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     LSFT,   A  , S  , D  , F  , G  ,                H  , J  , K  , L  ,SCLN,QUOT,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
     DOWN, Z  , X  , C  , V  , B  ,TAB,     LGUI, N  , M  ,COMM,DOT ,SLSH,RGHT,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                       LOWR,LCTL,SPC ,         ENT ,LALT,BSPC
  //                  `----+----+----'        `----+----+----'
  ),

  [_LOWER] = KEYMAP(
//,------+------+------+------+------+------.                ,------+------+------+------+------+------.
   ______, FR_1 , FR_2 , FR_3 , FR_4 , FR_5 ,                  FR_6 , FR_7 , FR_8 , FR_9 , FR_0 ,______,
//|------+------+------+------+------+------|                |------+------+------+------+------+------|
   ______,FR_AGRV,FR_CCED,FR_AMP,FR_PIPE,FR_EGRV,           FR_MINS,FR_LCBR,FR_RCBR,FR_PLUS,FR_AT,______,
//|------+------+------+------+------+------|                |------+------+------+------+------+------|
   ______,FR_UGRV,FR_LESS,FR_GRTR,FR_SLSH,FR_EURO,          FR_UNDS,FR_LPRN,FR_RPRN,FR_EQL,FR_DLR,FR_PERC,
//|------+------+------+------+------+------+------.  ,------+------+------+------+------+------+------|
______,S(FR_UGRV),FR_TILD,FR_GRV,FR_BSLS,S(KC_LBRC),______,______,FR_QUOT,FR_LBRC,FR_RBRC,FR_ASTR,FR_HASH,______,
//|------+------+------+------+------+------+------/  \------+------+------+------+------+------+------|
                              ______,______,______,     ______,______,______
//                                `----+----+----'      `----+----+----'
  ),

  [_RAISE] = KC_KEYMAP(
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
     F12 , F1 , F2 , F3 , F4 , F5 ,                F6 , F7 , F8 , F9 ,F10 ,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         ,EXLM, AT ,HASH,DLR ,PERC,               CIRC,AMPR,ASTR,LPRN,RPRN,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         ,MPRV,MNXT,VOLU,PGUP,UNDS,               EQL ,HOME,    ,    ,    ,BSLS,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
     MUTE,MSTP,MPLY,VOLD,PGDN,MINS,    ,         ,PLUS,END ,    ,    ,    ,    ,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                           ,    ,    ,             ,    ,    
  //                  `----+----+----'        `----+----+----'
  ),

  [_ADJUST] = KEYMAP(
  //,--------+--------+--------+--------+--------+--------.                          ,--------+--------+--------+--------+--------+--------.
      _______, _______, _______, _______, _______, _______,                            _______, _______, _______, _______, _______, _______,
  //|--------+--------+--------+--------+--------+--------|                          |--------+--------+--------+--------+--------+--------|
      RGB_TOG, QWERTY , BEPO   , RGB_SAI, RGB_VAI, _______,                            _______, _______, _______, _______, _______, _______,
  //|--------+--------+--------+--------+--------+--------|                          |--------+--------+--------+--------+--------+--------|
      _______, DEBUG  , RGB_HUD, RGB_SAD, RGB_VAD, _______,                            _______, _______, _______, _______, _______, _______,
  //|--------+--------+--------+--------+--------+--------+--------.        ,--------|--------+--------+--------+--------+--------+--------|
      BL_STEP, RESET  , _______, _______, _______, _______, _______,          _______, _______, _______, _______, _______, _______, _______,
  //`--------+--------+--------+----+---+--------+--------+--------/        \--------+--------+--------+---+----+--------+--------+--------'
                                      _______, _______, _______,                  _______, _______, _______
  //                                `--------+--------+--------'                `--------+--------+--------'
  )

};

#ifdef AUDIO_ENABLE
float tone_qwerty[][2]     = SONG(QWERTY_SOUND);
#endif

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        #ifdef AUDIO_ENABLE
          PLAY_SONG(tone_qwerty);
        #endif
        persistent_default_layer_set(1UL<<_QWERTY);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_LOWER);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      } else {
        layer_off(_RAISE);
        update_tri_layer(_LOWER, _RAISE, _ADJUST);
      }
      return false;
      break;
    case ADJUST:
      if (record->event.pressed) {
        layer_on(_ADJUST);
      } else {
        layer_off(_ADJUST);
      }
      return false;
      break;
    case BEPO:
      if (record->event.pressed) {
        persistent_default_layer_set(1UL<<_BEPOW);
      }
      return false;
      break;
    case BEPOS:
      if (record->event.pressed) {
        layer_on(_BEPOW_S);
      } else {
        layer_off(_BEPOW_S);
      }
      return false;
      break;
  }
  return true;
}
